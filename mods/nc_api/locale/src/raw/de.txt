msgid ""
msgstr ""
"PO-Revision-Date: 2022-05-11 06:20+0000\n"
"Last-Translator: kuboid <community@radtournetz.de>\n"
"Language-Team: German <https://hosted.weblate.org/projects/minetest/nodecore/"
"de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1\n"

msgid "- Drop items onto ground to create stack nodes. They do not decay."
msgstr ""
"- Wirf Gegenstände auf den Boden, um beständige Blockstapel zu erzeugen."

msgid "- Items picked up try to fit into the current selected slot first."
msgstr ""
"- Aufgehobene Dinge werden in die gerade ausgewählte Tasche gesteckt, falls "
"möglich."

msgid "- Larger recipes are usually more symmetrical."
msgstr "- Größere Baumuster sind meist symmetrisch."

msgid "- Learn to use the stars for long distance navigation."
msgstr "- Lerne die Sterne für Langstreckennavigation zu nutzen."

msgid "- "Furnaces" are not a thing; discover smelting with open flames."
msgstr "- \"Öfen\" gibt es nicht; entdecke Schmelzen mit offenem Feuer."

msgid "- DONE: @1"
msgstr "- ERLEDIGT: @1"

msgid "- Do not use F5 debug info; it will mislead you!"
msgstr "- Nutze nicht F5 für Entwicklerinformationen, es wird dich irreführen!"

msgid "- If it takes more than 5 seconds to dig, you don't have the right tool."
msgstr ""
"- Wenn es mehr als 5 Sekunden zum Abbauen braucht, hast du nicht das "
"richtige Werkzeug."

msgid "- Nodes dug without the right tool cannot be picked up, only displaced."
msgstr ""
"- Blöcke, die nicht mit dem richtigen Werkzeug abgebaut wurden, können nicht "
"aufgehoben, sondern nur verschoben werden."

msgid "- Ores may be hidden, but revealed by subtle clues in terrain."
msgstr "- Erze mögen versteckt sein, aber das umgebende Gelände gibt Hinweise."

msgid "- Trouble lighting a fire? Try using longer sticks, more tinder."
msgstr ""
"- Probleme mit dem Entzünden eines Feuers? Versuche längere Stöcke und mehr "
"Zunder."

msgid "write on a surface with a charcoal lump"
msgstr "schreibe auf eine Oberfläche mit Kohleklumpen"

msgid "- The game is challenging by design, sometimes frustrating. DON'T GIVE UP!"
msgstr ""
"- Das Spiel ist herausfordernd, manchmal frustrierend entworfen. GIB NICHT "
"AUF!"

msgid "Blue Cup Flower"
msgstr "Blaue Becherblume"

msgid "HAND OF POWER"
msgstr "MÄCHTIGE HAND"

msgid "Grass"
msgstr "Gras"

msgid "Gravel"
msgstr "Kies"

msgid "go for a swim"
msgstr "geh Schwimmen"

msgid "find charcoal"
msgstr "finde Holzkohle"

msgid "find a sponge"
msgstr "finde einen Schwamm"

msgid "find a flower"
msgstr "finde eine Blume"

msgid "dry out a sponge"
msgstr "Trockne einen Schwamm"

msgid "find a stick"
msgstr "finde einen Stock"

msgid "find ash"
msgstr "finde Asche"

msgid "hold your breath"
msgstr "halte deinen Atem an"

msgid "light a torch"
msgstr "entzünde eine Fackel"

msgid "drop an item"
msgstr "Lass einen Gegenstand fallen"

msgid "Growing Tree Trunk"
msgstr "Wachsender Baumstamm"

msgid "About"
msgstr "Über"

msgid "(C)2018-2021 by Aaron Suen <warr1024@@gmail.com>"
msgstr "(C)2018-2021 Aaron Suen <warr1024@@gmail.com>"

msgid "+"
msgstr "+"

msgid "- Aux+drop any item to drop everything."
msgstr "- Aux+Abwurf um alles fallen zu lassen."

msgid "- @1"
msgstr "- @1"

msgid "- Climbing spots may be climbed once black particles appear."
msgstr ""
"- Kletterstellen können erklommen werden, sobald schwarze Partikel "
"erscheinen."

msgid "- Climbing spots also produce very faint light; raise display gamma to see."
msgstr ""
"- Kletterstellen erzeugen sehr schwaches Licht; erhöhe deine "
"Bildschirmhelligkeit um zu sehen."

msgid "- Can't dig trees or grass? Search for sticks in the canopy."
msgstr ""
"- Nicht in der Lage Bäume oder Gras abzubauen? Suche Stöcke in den "
"Baumkronen."

msgid "- Order and specific face of placement may matter for crafting."
msgstr ""
"- Reihenfolge und Orientierung der Platzierungen können fürs Werken wichtig "
"sein."

msgid "- Drop and pick up items to rearrange your inventory."
msgstr ""
"- Lasse Gegenstände fallen und hebe sie wieder auf, um dein Inventar "
"umzuordnen."

msgid "- Crafting is done by building recipes in-world."
msgstr "- Werken funktioniert, indem man Baumuster in der Welt aufbaut."

msgid "- For larger recipes, the center item is usually placed last."
msgstr ""
"- Bei größeren Baumustern wird der mittlere Gegenstand für gewöhnlich "
"zuletzt platziert."

msgid "- If a recipe exists, you will see a special particle effect."
msgstr ""
"- Wenn ein Baumuster existiert, wirst du einen speziellen Partikeleffekt "
"sehen."

msgid "- Recipes are time-based, punching faster does not speed up."
msgstr "- Baumuster sind zeitabhängig, schneller schlagen hilft nicht."

msgid "- There is NO inventory screen."
msgstr "- Es gibt KEIN Inventarfenster."

msgid "- Some recipes use a 3x3 "grid", laid out flat on the ground."
msgstr ""
"- Manche Anleitungen verwenden ein 3x3 \"Gitter\", flach auf dem Boden "
"liegend."

msgid "@1 (100@2)"
msgstr "@1 (100@2)"

msgid "@1 (10@2)"
msgstr "@1 (10@2)"

msgid "@1 (2)"
msgstr "@1 (2)"

msgid "@1 (20@2)"
msgstr "@1 (20@2)"

msgid "@1 (3)"
msgstr "@1 (3)"

msgid "Annealed Lode"
msgstr "Geglühtes Lode"

msgid "@1 (30@2)"
msgstr "@1 (30@2)"

msgid "@1 (4)"
msgstr "@1 (4)"

msgid "@1 (40@2)"
msgstr "@1 (40@2)"

msgid "@1 (5)"
msgstr "@1 (5)"

msgid "Ash"
msgstr "Asche"

msgid "Ash Lump"
msgstr "Ascheklumpen"

msgid "Growing Leaves"
msgstr "Wachsende Blätter"

msgid "Hints"
msgstr "Hinweise"

msgid "Humus"
msgstr "Humus"

msgid "Wooden Frame"
msgstr "Holzrahmen"

msgid "Wet Sponge"
msgstr "Feuchter Schwamm"

msgid "Water"
msgstr "Wasser"

#, fuzzy
msgid "Amalgamation"
msgstr "Legierung"

msgid "Wooden Ladder"
msgstr "Holzleiter"

msgid "- Be wary of dark caves/chasms; you are responsible for getting yourself out."
msgstr ""
"- Hüte dich vor dunklen Höhlen oder Kluften; du musst selbst wieder "
"herausfinden."

msgid "Annealed Lode Mallet"
msgstr "Geglühter Lodehammer"

msgid "- Hold/repeat right-click on walls/ceilings barehanded to climb."
msgstr "- Halte oder rechtsklicke wiederholt auf Wände/Decken um zu klettern."

msgid "Annealed Lode Prill"
msgstr "Geglühter Lodebrocken"

msgid "Molten Glass"
msgstr "Geschmolzenes Glas"

#, fuzzy
msgid "Not all game content is covered by challenges. Explore!"
msgstr "Nicht aller Spielinhalt wird von Aufgaben abgedeckt. Erforsche!"

msgid "Pliant Tarstone"
msgstr "Weicher Teerstein"

msgid "- Hopelessly stuck? Try asking the community chatrooms (About tab)."
msgstr "- Total aufgeschmissen? Frag doch die Gemeinde (im \"Über\"-Tab)."

msgid "- Sneak+aux+drop an item to drop all matching items."
msgstr ""
"- Schleichen+Sonder+Abwurf drücken, um alle gleichartigen Dinge abzuwerfen."

msgid "- Sneak+drop to count out single items from stack."
msgstr ""
"- Schleichen+Abwurf drücken, um ein oder mehrere Dinge aus dem Stapel zu "
"nehmen."

msgid "- Some recipes require "pummeling" a node."
msgstr "- Einige Baumuster erfordern, einen Block zu behauen."

msgid "- Stacks may be pummeled, exact item count may matter."
msgstr ""
"- Stapel können behauen werden, die Anzahl der Dinge kann ausschlaggebend "
"sein."

msgid "- To pummel, punch a node repeatedly, WITHOUT digging."
msgstr "- Um zu behauen, schlage den Block wiederholt, OHNE zu graben."

msgid "- To run faster, walk/swim forward or climb/swim upward continuously."
msgstr "- Um schneller zu werden, laufe/schwimme/klettere dauerhaft."

msgid "- Tools used as ingredients must be in very good condition."
msgstr ""
"- Um Werkzeuge als Zutaten zu verwenden, müssen sie in gutem Zustand sein."

msgid "- Wielded item, target face, and surrounding nodes may matter."
msgstr ""
"- Das Werkzeug, die bearbeitete Blockseite und umgebende Blöcke machen was "
"aus."

msgid "- You do not have to punch very fast (about 1 per second)."
msgstr "- Du musst nicht schnell schlagen, einmal die Sekunde reicht."

msgid "Red Bell Flower"
msgstr "Rote Glockenblume"

msgid "@1 (50@2)"
msgstr "@1 (50@2)"

msgid "@1 (6)"
msgstr "@1 (6)"

msgid "@1 (60@2)"
msgstr "@1 (60@2)"

msgid "@1 (7)"
msgstr "@1 (7)"

msgid "@1 (70@2)"
msgstr "@1 (70@2)"

msgid "@1 (8)"
msgstr "@1 (8)"

msgid "@1 (80@2)"
msgstr "@1 (80@2)"

msgid "@1 (9)"
msgstr "@1 (9)"

msgid "@1 (90@2)"
msgstr "@1 (90@2)"

msgid "@1 ....."
msgstr "@1 ....."

msgid "@1 |...."
msgstr "@1 |...."

msgid "@1 ||..."
msgstr "@1 ||..."

msgid "@1 |||.."
msgstr "@1 |||.."

msgid "@1 ||||."
msgstr "@1 ||||."

msgid "@1 |||||"
msgstr "@1 |||||"

msgid "Active Lens"
msgstr "Aktive Linse"

msgid "Active Prism"
msgstr "Aktives Prisma"

msgid "Additional Mods Loaded: @1"
msgstr "Zusätzlich geladene Mods: @1"

msgid "Adobe"
msgstr "Lehm"

msgid "Adobe Bricks"
msgstr "Lehmziegel"

msgid "Adobe Mix"
msgstr "Lehmmischung"

msgid "Aggregate"
msgstr "Betonkies"

msgid "Annealed Lode Adze"
msgstr "Geglühte Lodehaue"

msgid "Annealed Lode Bar"
msgstr "Geglühter Lode-Barren"

msgid "Annealed Lode Frame"
msgstr "Geglühter Loderahmen"

msgid "Annealed Lode Hatchet"
msgstr "Geglühtes Lodebeil"

msgid "Annealed Lode Hatchet Head"
msgstr "Geglühter Lodebeilkopf"

msgid "Annealed Lode Ladder"
msgstr "Geglühte Lodeleiter"

msgid "Annealed Lode Mallet Head"
msgstr "Geglühter Lodehammerkopf"

msgid "Annealed Lode Mattock"
msgstr "Geglühte Lodehacke"

msgid "Annealed Lode Mattock Head"
msgstr "Geglühter Lodehackenkopf"

msgid "Annealed Lode Pick"
msgstr "Geglühter Lodepickel"

msgid "Annealed Lode Pick Head"
msgstr "Geglühter Lodepickelkopf"

msgid "Annealed Lode Rake"
msgstr "Geglühter Lodeharke"

msgid "Annealed Lode Rod"
msgstr "Geglühte Lodestange"

msgid "Annealed Lode Spade"
msgstr "Geglühter Lodespaten"

msgid "Annealed Lode Spade Head"
msgstr "Geglühter Lodespatenkopf"

msgid "Azure Bell Flower"
msgstr "Azurblaue Glockenblume"

msgid "See included LICENSE file for full details and credits"
msgstr "Alle Details und Verdienste siehe beigefügte LICENSE-Datei"

msgid "Burning Embers"
msgstr "Glut"

msgid "CHEATS ENABLED"
msgstr "SCHUMMELN MÖGLICH"

#, fuzzy
msgid "Challenges"
msgstr "Herausforderungen"

msgid "Charcoal"
msgstr "Holzkohle"

msgid "Charcoal Lump"
msgstr "Holzkohleklumpen"

msgid "Chromatic Glass"
msgstr "Lichtbrechendes Glas"

msgid "Clear Glass"
msgstr "Klarglas"

msgid "Clear Glass Case"
msgstr "Klarglasgehäuse"

msgid "Cobble"
msgstr "Bruchstein"

msgid "Cobble Hinged Panel"
msgstr "Bruchsteinklappe"

msgid "Cobble Panel"
msgstr "Bruchsteinplatte"

msgid "Cracked Stone"
msgstr "Gerissener Stein"

msgid "Crafting"
msgstr "Werken"

msgid "Crude Glass"
msgstr "Rohglas"

msgid "DEVELOPMENT VERSION"
msgstr "ENTWICKLER-VERSION"

msgid "Dirt"
msgstr "Dreck"

msgid "Discord: https://discord.gg/NNYeF6f"
msgstr "Discord: https://discord.gg/NNYeF6f"

msgid "Displaced Node"
msgstr "Versetzter Block"

msgid "Dry Rush"
msgstr "Trockene Binsen"

msgid "Eggcorn"
msgstr "Eichel"

msgid "Fire"
msgstr "Feuer"

msgid "Float Glass"
msgstr "Floatglas"

msgid "Float Glass Case"
msgstr "Floatglasgehäuse"

msgid "Flux"
msgstr "Flussmittel"

msgid "Glowing Lode"
msgstr "Glühendes Lode"

msgid "Glowing Lode Adze"
msgstr "Glühende Lodehaue"

msgid "Glowing Lode Bar"
msgstr "Glühender Lodebarren"

msgid "Glowing Lode Cobble"
msgstr "Glühender Lodebruchstein"

msgid "Glowing Lode Frame"
msgstr "Glühender Loderahmen"

msgid "Glowing Lode Hatchet Head"
msgstr "Glühender Lodebeilkopf"

msgid "Glowing Lode Ladder"
msgstr "Glühende Lodeleiter"

msgid "Glowing Lode Mallet Head"
msgstr "Glühender Lodehammerkopf"

msgid "Glowing Lode Mattock Head"
msgstr "Glühender Lodehackenkopf"

msgid "Glowing Lode Pick Head"
msgstr "Glühender Lodepickelkopf"

msgid "Glowing Lode Prill"
msgstr "Glühender Lodebrocken"

msgid "Glowing Lode Rake"
msgstr "Glühende Lodeharke"

msgid "Glowing Lode Rod"
msgstr "Glühende Lodestange"

msgid "Glowing Lode Spade Head"
msgstr "Glühender Lodespatenkopf"

msgid "Graveled Adze"
msgstr "Gekörnte Haue"

msgid "Inventory"
msgstr "Vorrat"

msgid "Leaves"
msgstr "Blätter"

msgid "Lens"
msgstr "Linse"

msgid "Lit Torch"
msgstr "Brennende Fackel"

msgid "Living Sponge"
msgstr "Lebender Schwamm"

msgid "Lode Cobble"
msgstr "Lodebruchstein"

msgid "Lode Crate"
msgstr "Lodekiste"

msgid "Lode Form"
msgstr "Lodeform"

msgid "Lode Ore"
msgstr "Lodeerz"

msgid "Log"
msgstr "Stamm"

msgid "Loose Cobble"
msgstr "Loser Bruchstein"

msgid "Loose Dirt"
msgstr "Loser Dreck"

msgid "Loose Gravel"
msgstr "Loser Kies"

msgid "Loose Humus"
msgstr "Loser Humus"

msgid "Loose Leaves"
msgstr "Lose Blätter"

msgid "Loose Lode Cobble"
msgstr "Loser Lode-Bruchstein"

msgid "Loose Sand"
msgstr "Loser Sand"

msgid "Movement"
msgstr "Bewegung"

msgid "Orange Bell Flower"
msgstr "Orangene Glockenblumen"

msgid "Peat"
msgstr "Torf"

msgid "Pink Bell Flower"
msgstr "Pinke Glockenblume"

msgid "Player's Guide: Crafting"
msgstr "Spielerhandbuch: Werken"

msgid "Player's Guide: Inventory Management"
msgstr "Spielerhandbuch: Vorratshaltung"

msgid "Player's Guide: Movement and Navigation"
msgstr "Spielerhandbuch: Bewegung und Navigation"

msgid "Player's Guide: Pummeling Recipes"
msgstr "Spielerhandbuch: Behauungsmuster"

msgid "Player's Guide: Tips and Guidance"
msgstr "Spielerhandbuch: Tipps und Hilfen"

msgid "Pliant Adobe"
msgstr "Weicher Lehm"

msgid "Pliant Sandstone"
msgstr "Weicher Sandstein"

msgid "Pliant Stone"
msgstr "Weicher Stein"

msgid "Prism"
msgstr "Prisma"

msgid "Progress: @1 complete, @2 current, @3 future"
msgstr "Fortschritt: @1 fertig, @2 momentan, @3 zukünftig"

msgid "Pumice"
msgstr "Bims"

msgid "Pummel"
msgstr "Behauen"

msgid "Raked Dirt"
msgstr "Geharkter Dreck"

msgid "Raked Gravel"
msgstr "Geharkter Kies"

msgid "Raked Humus"
msgstr "Geharkter Humus"

msgid "Raked Sand"
msgstr "Geharkter Sand"

msgid "Red Cluster Flower"
msgstr "Rote Büschelblume"

msgid "Red Cup Flower"
msgstr "Rote Becherblume"

msgid "Red Rosette Flower"
msgstr "Rote Rosettenblume"

msgid "Red Star Flower"
msgstr "Rote Sternblume"

msgid "Render"
msgstr "Putz"

msgid "Rush"
msgstr "Binse"

msgid "Sand"
msgstr "Sand"

msgid "Sandstone"
msgstr "Sandstein"

msgid "Sandstone Bricks"
msgstr "Sandsteinziegel"

msgid "Sedge"
msgstr "Riedgras"

msgid "Shining Lens"
msgstr "Scheinende Linse"

msgid "Sponge"
msgstr "Schwamm"

msgid "Sprout"
msgstr "Spross"

msgid "Staff"
msgstr "Stab"

msgid "Stick"
msgstr "Stock"

msgid "Stone"
msgstr "Stein"

msgid "Stone Bricks"
msgstr "Steinziegel"

msgid "Stone Chip"
msgstr "Steinsplitter"

msgid "Stone-Tipped Hatchet"
msgstr "Steingekörntes Beil"

msgid "Stone-Tipped Mallet"
msgstr "Steingekörnter Hammer"

msgid "Stone-Tipped Pick"
msgstr "Steingekörnter Pickel"

msgid "Stone-Tipped Spade"
msgstr "Steinbespitzter Spaten"

msgid "Stone-Tipped Stylus"
msgstr "Griffel mit Steinspitze"

msgid "Stump"
msgstr "Baumstumpf"

msgid "Tarstone"
msgstr "Teerstein"

msgid "Tarstone Bricks"
msgstr "Teersteinziegel"

msgid "Tempered Lode"
msgstr "Veredeltes Lode"

msgid "Tempered Lode Adze"
msgstr "Veredelte Lodehaue"

msgid "Tempered Lode Bar"
msgstr "Veredelter Lodebarren"

msgid "Tempered Lode Frame"
msgstr "Veredelter Loderahmen"

msgid "Tempered Lode Hatchet"
msgstr "Veredeltes Lodebeil"

msgid "Tempered Lode Hatchet Head"
msgstr "Veredelter Lodebeilkopf"

msgid "Tempered Lode Ladder"
msgstr "Veredelte Lodeleiter"

msgid "Tempered Lode Mallet"
msgstr "Veredelte Lodehammer"

msgid "Tempered Lode Mallet Head"
msgstr "Veredelter Lodehammerkopf"

msgid "Tempered Lode Mattock"
msgstr "Veredelte Lodehacke"

msgid "Tempered Lode Mattock Head"
msgstr "Veredelter Lodehackenkopf"

msgid "Tempered Lode Pick"
msgstr "Veredelter Lodepickel"

msgid "Tempered Lode Pick Head"
msgstr "Veredelter Lodepickelkopf"

msgid "Tempered Lode Prill"
msgstr "Veredelter Lodebrocken"

msgid "Tempered Lode Rake"
msgstr "Veredelte Lodeharke"

msgid "Tempered Lode Rod"
msgstr "Veredelte Lodestange"

msgid "Tempered Lode Spade"
msgstr "Veredelter Lodespaten"

msgid "Tempered Lode Spade Head"
msgstr "Veredelter Lodespatenkopf"

msgid "Thatch"
msgstr "Stroh"

msgid "Tips"
msgstr "Tipps"

msgid "Torch"
msgstr "Fackel"

msgid "Tote (@1 / @2)"
msgstr "Tragetasche (@1 / @2)"

msgid "Tree Trunk"
msgstr "Baumstamm"

msgid "Violet Bell Flower"
msgstr "Lila Glockenblume"

msgid "Violet Cluster Flower"
msgstr "Lila Büschelblume"

msgid "Violet Cup Flower"
msgstr "Lila Becherblume"

msgid "Violet Rosette Flower"
msgstr "Lila Rosettenblume"

msgid "Violet Star Flower"
msgstr "Lila Sternblume"

msgid "Wet Adobe Mix"
msgstr "Nasse Lehmmischung"

msgid "Wet Aggregate"
msgstr "Nasser Betonkies"

msgid "Wet Render"
msgstr "Nasser Putz"

msgid "Wet Tarry Aggregate"
msgstr "Nasser Teerbetonkies"

msgid "White Bell Flower"
msgstr "Weiße Glockenblume"

msgid "White Cluster Flower"
msgstr "Weiße Büschelblume"

msgid "White Cup Flower"
msgstr "Weiße Becherblume"

msgid "White Rosette Flower"
msgstr "Weiße Rosettenblume"

msgid "White Star Flower"
msgstr "Weiße Sternblume"

msgid "Wicker"
msgstr "Korb"

msgid "Wooden Adze"
msgstr "Holzhaue"

msgid "Wooden Form"
msgstr "Holzform"

msgid "Wooden Hatchet"
msgstr "Holzbeil"

msgid "Wooden Hatchet Head"
msgstr "Holzbeilkopf"

msgid "Wooden Hinged Panel"
msgstr "Holzklappe"

msgid "Wooden Mallet"
msgstr "Holzhammer"

msgid "Wooden Mallet Head"
msgstr "Holzhammerkopf"

msgid "Wooden Panel"
msgstr "Holzplatte"

msgid "Wooden Pick"
msgstr "Holzpickel"

msgid "Wooden Pick Head"
msgstr "Holzpickelkopf"

msgid "Wooden Plank"
msgstr "Holzplanke"

msgid "breed a new flower variety"
msgstr "züchte eine neue Blumenvariante"

msgid "Wooden Rake"
msgstr "Holzharke"

msgid "Wooden Shelf"
msgstr "Holzregal"

msgid "Wooden Spade"
msgstr "Holzspaten"

msgid "Wooden Spade Head"
msgstr "Holzspatenkopf"

msgid "Yellow Bell Flower"
msgstr "Gelbe Glockenblume"

msgid "Yellow Cluster Flower"
msgstr "Gelbe Büschelblume"

msgid "Yellow Cup Flower"
msgstr "Gelbe Becherblume"

msgid "Yellow Rosette Flower"
msgstr "Gelbe Rosettenblume"

msgid "Yellow Star Flower"
msgstr "Gelbe Sternblume"

msgid "activate a lens"
msgstr "aktiviere eine Linse"

msgid "activate a prism"
msgstr "aktivere ein Prisma"

msgid "add coal to aggregate to make tarstone"
msgstr "füge Kohle zum Betonkies hinzu, um Teerstein herzustellen"

msgid "assemble a clear glass case"
msgstr "baue ein Klarglasgehäuse"

msgid "assemble a float glass case"
msgstr "baue ein Floatglasgehäuse"

msgid "assemble a lode adze"
msgstr "baue eine Lodehaue"

msgid "assemble a lode rake"
msgstr "Baue eine Lodeharke"

msgid "assemble a rake from adzes and a stick"
msgstr "baue einen Rechen aus Hauen und einem Stock"

msgid "assemble a staff from sticks"
msgstr "baue einen Stab aus Stöcken"

msgid "assemble a stone-tipped stylus"
msgstr "baue einen steinbesetzten Griffel"

msgid "assemble a wooden frame from staves"
msgstr "baue einen Holzrahmen aus Stäben"

msgid "assemble a wooden ladder from sticks"
msgstr "baue eine Holzleiter aus Stöcken"

msgid "assemble a wooden shelf from frames and planks"
msgstr "baue ein Holzregal aus Rahmen und Planken"

msgid "assemble a wooden tool"
msgstr "baue ein Holzwerkzeug"

msgid "assemble an adze out of sticks"
msgstr "setze eine Haue aus Stöcken zusammen"

msgid "bash a plank into sticks"
msgstr "zerschlage eine Planke in Stöcke"

msgid "bond adobe bricks"
msgstr "binde Lehmziegel"

msgid "bond sandstone bricks"
msgstr "binde Sandsteinziegel"

msgid "bond stone bricks"
msgstr "binde Steinziegel"

msgid "bond tarstone bricks"
msgstr "binde Teersteinziegel"

msgid "break cobble into chips"
msgstr "zersplittere Bruchstein"

msgid "carve a wooden plank completely"
msgstr "zerschnitze eine Holzplanke vollständig"

msgid "carve wooden tool heads from planks"
msgstr "schnitze Holzwerkzeuge aus Planken"

msgid "catapult an item with a hinged panel"
msgstr "schleudere etwas mit einer Klappe"

msgid "@1 discovered, @2 available, @3 future"
msgstr "@1 entdeckt, @2 entdeckbar, @3 zukünftige"

msgid "- Displaced nodes can be climbed through like climbing spots."
msgstr "- Versetzte Blöcke können durchklettert werden wie Kletterstellen."

msgid "Bindy"
msgstr "Bündig"

msgid "Bindy Adobe"
msgstr "Bündiger Lehm"

msgid "Bindy Pliant Adobe"
msgstr "Bündiger Weicher Lehm"

msgid "Bindy Pliant Sandstone"
msgstr "Bündiger Weicher Sandstein"

msgid "Bindy Pliant Stone"
msgstr "Bündiger Weicher Stein"

msgid "Bindy Pliant Tarstone"
msgstr "Bündiger Weicher Teerstein"

msgid "Bindy Sandstone"
msgstr "Bündiger Sandstein"

msgid "Bindy Stone"
msgstr "Bündiger Stein"

msgid "Bindy Tarstone"
msgstr "Bündiger Teerstein"

msgid "Blank"
msgstr "Leer"

msgid "Boxy"
msgstr "Kastig"

msgid "Boxy Adobe"
msgstr "Kastiger Lehm"

msgid "Boxy Pliant Adobe"
msgstr "Kastiger Weicher Lehm"

msgid "Boxy Pliant Sandstone"
msgstr "Kastiger Weicher Sandstein"

msgid "Boxy Pliant Stone"
msgstr "Kastiger Weicher Stein"

msgid "Boxy Pliant Tarstone"
msgstr "Kastiger Weicher Teerstein"

msgid "Boxy Sandstone"
msgstr "Kastiger Sandstein"

msgid "Boxy Stone"
msgstr "Kastiger Stein"

msgid "Boxy Tarstone"
msgstr "Kastiger Teerstein"

msgid "Bricky"
msgstr "Maurig"

msgid "Bricky Adobe"
msgstr "Mauriger Lehm"

msgid "Bricky Pliant Adobe"
msgstr "Mauriger Weicher Lehm"

msgid "Bricky Pliant Sandstone"
msgstr "Mauriger Weicher Sandstein"

msgid "Bricky Pliant Stone"
msgstr "Mauriger Weicher Stein"

msgid "Bricky Pliant Tarstone"
msgstr "Mauriger Weicher Teerstein"

msgid "Bricky Sandstone"
msgstr "Mauriger Sandstein"

msgid "Bricky Stone"
msgstr "Mauriger Stein"

msgid "Bricky Tarstone"
msgstr "Mauriger Teerstein"

msgid "sinter glowing lode prills into a cube"
msgstr "schmelze glühende Lodebrocken zu einem Block"

msgid "solder lode rods into crates"
msgstr "löte Lodestäbe zu Kisten"

msgid "split a tree trunk into planks"
msgstr "spalte einen Baumstamm zu Planken"

msgid "squeeze out a sponge"
msgstr "quetsche einen Schwamm aus"

msgid "temper a lode cube"
msgstr "veredle einen Lodeblock"

msgid "temper a lode tool head"
msgstr "veredle einen Lode-Werkzeugkopf"

msgid "throw an item really fast"
msgstr "wirf etwas so richtig schnell"

msgid "weaken stone by soaking"
msgstr "schwäche Stein über Durchnässung"

msgid "weld glowing lode pick and spade heads together"
msgstr "schmiede glühende Pickel- und Spatenköpfe zusammen"

msgid "wet a concrete mix"
msgstr "befeuchte eine Betonmischung"

msgid "work annealed lode on a tempered lode anvil"
msgstr "bearbeite geglühtes Lode auf einem veredelten Lodeamboss"

msgid "work glowing lode on a lode anvil"
msgstr "bearbeite glühendes Lode auf einem Lodeamboss"

msgid "work glowing lode on a stone anvil"
msgstr "bearbeite glühendes Lode auf einem Steinamboss"

msgid "Blue Star Flower"
msgstr "Blaue Sternblume"

msgid "Infused Annealed Lode Adze"
msgstr "Aufgegossene Geglühte Lodehaue"

msgid "Odo Charcoal Glyph"
msgstr "Odo-Holzkohlezeichen"

msgid "Orange Cluster Flower"
msgstr "Orangene Büschelblume"

msgid "Qeg Charcoal Glyph"
msgstr "Qeg-Holzkohlezeichen"

msgid "assemble a lode tote handle"
msgstr "baue einen Lodetaschengriff"

msgid "assemble a wooden shelf from a from and plank"
msgstr "baue ein Holzregal aus einem Rahmen und einer Planke"

msgid "assemnble a lode crate from form and bar"
msgstr "Baue eine Lodekiste aus Rahmen und Barren"

msgid "convert a wooden frame to a form"
msgstr "Wandle einen Holzrahmen in eine Form"

msgid "convert a wooden form to a frame"
msgstr "Wandle eine Holzform in einen Rahmen"

msgid "dig up a tree stump"
msgstr "Grabe einen Baumstumpf aus"

msgid "dig up cobble"
msgstr "Grabe Bruchstein aus"

msgid "discovered - @1"
msgstr "entdeckt - @1"

msgid "dry out a rush"
msgstr "Trockne eine Binse"

msgid "etch pliant concrete with a stylus"
msgstr "verziere weichen Beton mit einem Griffel"

msgid "ferment peat into humus"
msgstr "fermentiere Torf zu Humus"

msgid "find a rush"
msgstr "Finde eine Binse"

msgid "Azure Cluster Flower"
msgstr "Azurblaue Büschelblume"

msgid "Azure Cup Flower"
msgstr "Azurblaue Becherblume"

msgid "Azure Rosette Flower"
msgstr "Azurblaue Rosettenblume"

msgid "Azure Star Flower"
msgstr "Azurblaue Sternblume"

msgid "Black Bell Flower"
msgstr "Schwarze Glockenblume"

msgid "Black Cluster Flower"
msgstr "Schwarze Büschelblume"

msgid "Black Cup Flower"
msgstr "Schwarze Becherblume"

msgid "Black Rosette Flower"
msgstr "Schwarze Rosettenblume"

msgid "Black Star Flower"
msgstr "Schwarze Sternblume"

msgid "Cav Charcoal Glyph"
msgstr "Cav-Holzkohlezeichen"

msgid "Fot Charcoal Glyph"
msgstr "Fot-Holzkohlezeichen"

msgid "Hashy"
msgstr "Kraus"

msgid "Iceboxy"
msgstr "Verschachtelt"

msgid "Iceboxy Adobe"
msgstr "Verschachtelter Lehm"

msgid "Iceboxy Pliant Adobe"
msgstr "Verschachtelter Weicher Lehm"

msgid "Iceboxy Pliant Sandstone"
msgstr "Verschachtelter Weicher Sandstein"

msgid "Iceboxy Pliant Stone"
msgstr "Verschachtelter Weicher Stein"

msgid "Iceboxy Pliant Tarstone"
msgstr "Verschachtelter Weicher Teerstein"

msgid "Iceboxy Sandstone"
msgstr "Verschachtelter Weicher Sandstein"

msgid "Iceboxy Stone"
msgstr "Verschachtelter Weicher Stein"

msgid "Iceboxy Tarstone"
msgstr "Verschachtelter Weicher Teerstein"

msgid "Infused Annealed Lode Hatchet"
msgstr "Aufgegossenes Geglühtes Lodebeil"

msgid "Infused Annealed Lode Mallet"
msgstr "Aufgegossener Geglühter Lodehammer"

msgid "Orange Cup Flower"
msgstr "Orangene Becherblume"

msgid "Orange Rosette Flower"
msgstr "Orangene Rosettenblume"

msgid "Orange Star Flower"
msgstr "Orangene Sternblume"

msgid "Pink Cluster Flower"
msgstr "Pinke Büschelblume"

msgid "Pink Cup Flower"
msgstr "Pinke Becherblume"

msgid "Pink Rosette Flower"
msgstr "Pinke Rosettenblume"

msgid "Pink Star Flower"
msgstr "Pinke Sternblume"

msgid "Prx Charcoal Glyph"
msgstr "Prx-Holzkohlezeichen"

#, fuzzy
msgid "Pumwater"
msgstr "Hauwasser"

msgid "Blue Bell Flower"
msgstr "Blaue Glockenblume"

msgid "Blue Cluster Flower"
msgstr "Blaue Büschelblume"

msgid "Blue Rosette Flower"
msgstr "Blaue Rosettenblume"

msgid "Discovery"
msgstr "Entdeckung"

msgid "Gated Prism"
msgstr "Steuerbares Prisma"

msgid "Geq Charcoal Glyph"
msgstr "Geq-Holzkohlezeichen"

msgid "Hashy Adobe"
msgstr "Krauser Lehm"

msgid "Hashy Pliant Adobe"
msgstr "Krauser Weicher Lehm"

msgid "Hashy Pliant Sandstone"
msgstr "Krauser Weicher Sandstein"

msgid "Hashy Pliant Stone"
msgstr "Krauser Weicher Stein"

msgid "Hashy Pliant Tarstone"
msgstr "Krauser Weicher Teerstein"

msgid "Hashy Sandstone"
msgstr "Krauser Sandstein"

msgid "Hashy Stone"
msgstr "Krauser Stein"

msgid "Hashy Tarstone"
msgstr "Krauser Teerstein"

msgid "Horzy"
msgstr "Querig"

msgid "Horzy Adobe"
msgstr "Queriger Lehm"

msgid "Horzy Pliant Adobe"
msgstr "Queriger Weicher Lehm"

msgid "Horzy Pliant Sandstone"
msgstr "Queriger Weicher Sandstein"

msgid "Horzy Pliant Stone"
msgstr "Queriger Weicher Stein"

msgid "Horzy Pliant Tarstone"
msgstr "Queriger Weicher Teerstein"

msgid "Horzy Sandstone"
msgstr "Queriger Sandstein"

msgid "Horzy Stone"
msgstr "Queriger Stein"

msgid "Horzy Tarstone"
msgstr "Queriger Teerstein"

msgid "Infused Annealed Lode Mattock"
msgstr "Aufgegossene Geglühte Lodehacke"

msgid "Infused Annealed Lode Pick"
msgstr "Aufgegosseneer Geglühter Lodepickel"

msgid "Infused Annealed Lode Rake"
msgstr "Aufgegossene Geglühte Lodeharke"

msgid "Infused Annealed Lode Spade"
msgstr "Aufgegossener Geglühter Lodespaten"

msgid "Infused Tempered Lode Adze"
msgstr "Aufgegossene Veredelte Lodehaue"

msgid "Infused Tempered Lode Hatchet"
msgstr "Aufgegossenes Veredeltes Lodebeil"

msgid "Infused Tempered Lode Mallet"
msgstr "Aufgegossener Veredelter Lodehammer"

msgid "Infused Tempered Lode Mattock"
msgstr "Aufgegossene Veredelte Lodehacke"

msgid "Infused Tempered Lode Pick"
msgstr "Aufgegossener Veredelter Lodepickel"

msgid "Infused Tempered Lode Rake"
msgstr "Aufgegossene Veredelte Lodeharke"

msgid "Infused Tempered Lode Spade"
msgstr "Aufgegossener Veredelter Lodespaten"

msgid "Loose Amalgamation"
msgstr "Lose Legierung"

msgid "Loose Lux Cobble"
msgstr "Loser Luxbruchstein"

msgid "Lux Cobble"
msgstr "Luxbruchstein"

msgid "Mew Charcoal Glyph"
msgstr "Mew-Holzkohlezeichen"

msgid "Niz Charcoal Glyph"
msgstr "Niz-Holzkohlezeichen"

msgid "MIT License (http://www.opensource.org/licenses/MIT)"
msgstr "MIT-Lizenz (http://www.opensource.org/licenses/MIT)"

msgid "Vermy Sandstone"
msgstr "Wirrer Sandstein"

msgid "Vermy Stone"
msgstr "Wirrer Stein"

msgid "Vermy Tarstone"
msgstr "Wirrer Teerstein"

msgid "Tof Charcoal Glyph"
msgstr "Tof-Holzkohlezeichen"

msgid "Vermy"
msgstr "Wirr"

msgid "Vermy Adobe"
msgstr "Wirrer Lehm"

msgid "Vermy Pliant Adobe"
msgstr "Wirrer Weicher Lehm"

msgid "Vermy Pliant Sandstone"
msgstr "Wirrer Weicher Sandstein"

msgid "Vermy Pliant Stone"
msgstr "Wirrer Weicher Stein"

msgid "Vermy Pliant Tarstone"
msgstr "Wirrer Weicher Teerstein"

msgid "Verty"
msgstr "Hochig"

msgid "Tote Handle"
msgstr "Taschengriff"

msgid "The discovery system only alerts you to the existence of some basic game mechanics. More advanced content, such as emergent systems and automation, you will have to invent yourself!"
msgstr ""
"Das Entdeckungssystem weist dich nur auf die Existenz einiger grundlegender "
"Spielmechaniken hin. Fortgeschrittenes wie weiterführende Systeme und "
"Automation wirst du selbst erfinden müssen!"

msgid "Verty Adobe"
msgstr "Hochiger Lehm"

msgid "Verty Pliant Adobe"
msgstr "Hochiger Weicher Lehm"

msgid "Verty Pliant Sandstone"
msgstr "Hochiger Weicher Sandstein"

msgid "Verty Pliant Stone"
msgstr "Hochiger Weicher Stein"

msgid "Verty Pliant Tarstone"
msgstr "Hochiger Weicher Teerstein"

msgid "Verty Sandstone"
msgstr "Hochiger Sandstein"

msgid "Verty Stone"
msgstr "Hochiger Stein"

msgid "Verty Tarstone"
msgstr "Hochiger Teerstein"

msgid "Wilted Bell Flower"
msgstr "Welke Glockenblume"

msgid "Wilted Cluster Flower"
msgstr "Welke Büschelblume"

msgid "Wilted Cup Flower"
msgstr "Welke Becherblume"

msgid "Wilted Rosette Flower"
msgstr "Welke Rosettenblume"

msgid "Wilted Star Flower"
msgstr "Welke Sternblume"

msgid "Xrp Charcoal Glyph"
msgstr "Xrp-Holzkohlezeichen"

msgid "Yit Charcoal Glyph"
msgstr "Yit-Holzkohlezeichen"

msgid "Zin Charcoal Glyph"
msgstr "Zin-Holzkohlezeichen"

msgid "anneal a lode cube"
msgstr "Glühe einen Lodewürfel"

msgid "change a stylus pattern"
msgstr "Ändere ein Griffelmuster"

msgid "chisel sandstone bricks"
msgstr "meißle Sandsteinziegel"

msgid "chisel stone bricks"
msgstr "meißle Steinziegel"

msgid "chisel adobe bricks"
msgstr "meißle Lehmziegel"

msgid "chisel tarstone bricks"
msgstr "meißle Teersteinziegel"

msgid "chop chromatic glass into lenses"
msgstr "zerschneide farbiges Glas in Linsen"

msgid "chop up charcoal"
msgstr "zerhacke Holzkohle"

msgid "complete a craft with a hinged panel"
msgstr "baue etwas mit einer Klappe"

msgid "complete a pummel with a hinged panel and tool head"
msgstr "vervollständige einen Hau mit einer Klappe und einem Werkzeugkopf"

msgid "cool molten glass into crude glass"
msgstr "kühle geschmolzenes Glas zu Rohglas"

msgid "craft a torch from staff and coal lump"
msgstr "Bastle eine Fackel aus einem Stab und einem Kohleklumpen"

msgid "cure pliant concrete fully"
msgstr "Härte weichen Beton voll aus"

msgid "cut down a tree"
msgstr "Fälle einen Baum"

msgid "cycle through all charcoal glyphs"
msgstr "Durchlaufe alle Holzkohlezeichen"

msgid "dig a node with a hinged panel and tool"
msgstr "Grabe einen Block mit einer Klappe und einem Werkzeug aus"

msgid "dig leaves"
msgstr "Grabe Blätter"

msgid "compress something with a hinged panel"
msgstr "Presse etwas mit einer Klappe"

msgid "dig up sand"
msgstr "Grabe Sand aus"

msgid "drop all your items at once"
msgstr "wirf alles auf einmal weg"

msgid "dig up dirt"
msgstr "Grabe Dreck aus"

msgid "dig up gravel"
msgstr "Grabe Kies aus"

msgid "dig up lode ore"
msgstr "Grabe Lodeerz aus"

msgid "dig up lux cobble"
msgstr "Grabe Luxbruchstein aus"

msgid "@1 (@2)"
msgstr "@1 (@2)"

msgid "see a tree grow"
msgstr "beobachte einen Baum beim wachsen"

msgid "Glued Shining Lens"
msgstr "geklebte scheinende Linse"

msgid "Glued Prism"
msgstr "geklebtes Prisma"

msgid "Glued Active Lens"
msgstr "geklebte aktive Linse"

msgid "Glued Active Prism"
msgstr "geklebtes aktives Prisma"

msgid "Glued Gated Prism"
msgstr "geklebtes geschaltetes Prisma"

msgid "Glued Lens"
msgstr "geklebte Linse"

msgid "chisel a hinge groove into a wooden plank"
msgstr "meißle eine Gelenkführung in eine Holzplanke"

msgid "harvest a sponge"
msgstr "ernte einen Schwamm"

msgid "grind dead plants into peat"
msgstr "mahle tote Pflanzen zu Torf"

msgid "grow a sedge on moist grass"
msgstr "züchte Riedgras auf feuchtem Gras"

msgid "rotate a charcoal glyph"
msgstr "drehe ein Holzkohlezeichen"

msgid "harden stone"
msgstr "härte Stein"

msgid "grow a flower on moist soil"
msgstr "züchte eine Pflanze auf feuchtem Boden"

msgid "grow a rush on moist soil"
msgstr "züchte eine Binse auf feuchtem Boden"

msgid "chisel a hinge groove into cobble"
msgstr "meißle eine Gelenkführung in Bruchstein"

msgid "mix gravel into ash to make aggregate"
msgstr "mische Kies in Asche um Betonkies herzustellen"

msgid "propel hinged panel with focused light"
msgstr "treibe eine Klappe durch gebündeltes Licht an"

msgid "push an item into a storage box with a hinged panel"
msgstr "schiebe etwas in einen Kasten mittels einer Klappe"

msgid "place a node with a hinged panel"
msgstr "platziere einen Block mittels einer Klappe"

msgid "wilt a flower"
msgstr "lass eine Blume verwelken"

msgid "find a sedge"
msgstr "finde Riedgras"

msgid "find dry (loose) leaves"
msgstr "finde trockene (lose) Blätter"

msgid "find lux"
msgstr "finde Lux"

msgid "find pumice"
msgstr "finde Torf"

msgid "forge a lode prill into a bar"
msgstr "schmiede einen Lode-Brocken zu einem Barren"

msgid "forge an annealed lode frame into a form"
msgstr "schmiede einen veredelten Loderahmen zu einer Form"

msgid "forge lode bars into a rod"
msgstr "schmiede Lodebarren zu einer Stange"

msgid "forge lode prills into a tool head"
msgstr "schmiede Lodebrocken zu einem Werkzeugkopf"

msgid "forge lode rods into a frame"
msgstr "schmiede Lodestangen zu einem Rahmen"

msgid "produce light from a lens"
msgstr "erzeuge Licht mit einer Linse"

msgid "put a gravel tip on a wooden adze"
msgstr "stecke einen Kiesel auf eine Holzhaue"

msgid "rake dirt"
msgstr "harke Dreck"

msgid "scale a wall"
msgstr "erklettere eine Wand"

msgid "set concrete to pliant"
msgstr "mache Beton formbar"

msgid "stick a lens/prism in place"
msgstr "bringe eine Linse oder ein Prisma on Stellung"

msgid "put a stone tip onto a wooden tool"
msgstr "versehe ein Holzwerkzeug mit einer Steinspitze"

msgid "rake gravel"
msgstr "harke Kies"

msgid "run at full speed"
msgstr "renne mit voller Geschwindigkeit"

msgid "scale an overhang"
msgstr "erklettere einen Überhang"

msgid "rake humus"
msgstr "harke Humus"

msgid "rake sand"
msgstr "harke Sand"

msgid "assemble a lode crate from form and bar"
msgstr "setze eine Lodekiste aus einer Form und einem Barren zusammen"

msgid "assemble a rake from adzes and a staff"
msgstr "setze eine Harke aus Hauen und einem Stab zusammen"

msgid "assemble a wooden shelf from a form and plank"
msgstr "setze ein Holzregal aus einer Form und einer Planke zusammen"

msgid "chip chromatic glass into prisms"
msgstr "Prismen aus chromatischem Glas herstellen"

msgid "find an eggcorn"
msgstr "finde eine Eichel"

msgid "find lode ore"
msgstr "finde Lode-Erz"

msgid "gate a prism"
msgstr "steuere ein Prisma an"

msgid "forge a lode rod and bar into a ladder"
msgstr "schmiede eine Lodestange und einen -barren zu einer Leiter"

msgid "leach raked dirt to sand"
msgstr "wasche geharkten Dreck zu Sand aus"

msgid "leach raked humus to dirt"
msgstr "wasche geharkten Humus zu Dreck aus"

msgid "Bonded Adobe Brick Panel"
msgstr "gebundene Lehmziegelklappe"

msgid "Bonded Adobe Brick Hinged Panel"
msgstr "gebundene gelagerte Lehmziegelklappe"

msgid "Bonded Adobe Bricks"
msgstr "gebundene Lehmziegel"

msgid "Bonded Sandstone Brick Hinged Panel"
msgstr "gebundene gelagerte Lehmziegelklappe"

msgid "Bonded Sandstone Brick Panel"
msgstr "gebundene Sandsteinziegelklappe"

msgid "Bonded Sandstone Bricks"
msgstr "gebundene Sandsteinziegel"

msgid "Bonded Stone Brick Hinged Panel"
msgstr "gebundene gelagerte Steinziegelklappe"

msgid "Bonded Stone Brick Panel"
msgstr "gebundene Steinziegelklappe"

msgid "Bonded Stone Bricks"
msgstr "gebundene Steinziegel"

msgid "Bonded Tarstone Brick Hinged Panel"
msgstr "gebundene gelagerte Teersteinziegelklappe"

msgid "Bonded Tarstone Brick Panel"
msgstr "gebundene Teersteinziegelklappe"

msgid "Bonded Tarstone Bricks"
msgstr "gebundene Teersteinziegel"

msgid "insert metal rod into a cobble panel"
msgstr "schiebe eine Metallstange in eine Bruchsteinklappe"

msgid "insert wooden pin into wooden panel"
msgstr "schiebe einen Holzstab in eine Holzklappe"

msgid "make fire by rubbing sticks together"
msgstr "entfach Feuer durch Reibung von Stöcken"

msgid "melt down lode metal from lode cobble"
msgstr "schmelze Lodemetall aus Lodebruchstein"

msgid "melt sand into molten glass"
msgstr "schmelze Sand zu Glasschmelze"

msgid "mix dirt into ash to make adobe mix"
msgstr "mische Dreck in Asche für Lehmmischung"

msgid "mix sand into ash to make render"
msgstr "mische Sand in Asche für Putz"

msgid "mold molten glass into clear glass"
msgstr "gieße Glasschmelze zu Klarglas"

msgid "mold molten glass into float glass"
msgstr "gieße Glasschmelze zu Floatglas"

msgid "navigate by touch in darkness"
msgstr "ertaste den Weg in Dunkelheit"

msgid "observe a lux reaction"
msgstr "beobachte eine Luxreaktion"

msgid "pack high-quality charcoal"
msgstr "stopfe hochqualitative Holzkohle zusammen"

msgid "pack sedges into thatch"
msgstr "stopfe Riedgras zu Stroh zusammen"

msgid "pack stone chips back into cobble"
msgstr "stopfe Steinbrocken zu Bruchstein zusammen"

msgid "pick a sedge"
msgstr "sammle Riedgras"

msgid "plant an eggcorn"
msgstr "pflanze eine Eichel"

msgid "quench molten glass into chromatic glass"
msgstr "kühle Glasschmelze zu lichtbrechendem Glas"

msgid "pack dry rushes into wicker"
msgstr "flechte Binsen"

msgid "- FUTURE: @1"
msgstr "- IN ZUKUNFT: @1"

msgid "Minetest's top original voxel game about emergent mechanics and exploration"
msgstr ""
"Minetests wahrhaftes Voxelspiel über entwickelnde Mechaniken und Erforschung"

msgid "Early-access edition of NodeCore with latest features (and maybe bugs)"
msgstr "Frühausgabe mit der neuesten Ausstattung (und evtl. Schnitzern)"

msgid "complete an assembly recipe with a hinged panel"
msgstr "vollende ein Baumuster mittels einer gelagerten Klappe"

msgid "find deep stone strata"
msgstr "entdecke Tiefengesteinsschichten"

msgid "lux-infuse a lode tool"
msgstr "beschichte ein Lode-Werkzeug mit Lux"

msgid "pack up a complete tote"
msgstr "stelle eine komplette Tasche zusammen"
