-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("convey")
include("operate")
include("ablation")
include("register")
include("craft_catapult")
include("craft_press")
include("hints")
