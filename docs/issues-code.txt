========================================================================
ISSUES-CODE: Issues related to code quality and APIs
------------------------------------------------------------------------

  ####   #####    ####      #    #       ######  #####    ####
 #       #    #  #    #     #    #       #       #    #  #
  ####   #    #  #    #     #    #       #####   #    #   ####
      #  #####   #    #     #    #       #       #####        #
 #    #  #       #    #     #    #       #       #   #   #    #
  ####   #        ####      #    ######  ######  #    #   ####

- Need to standardize storage API between storeboxes and bare stacks
	- Storebox API is a mess anyway and needs a lot of cleanup.

- If DNTs are deferred due to either stasis or arealoaded, need to
  set the timer re-firing to some sane future time.

- Unified heat API?
	- Unify quenching and heating
	- Alternative to cooking w/ 3 flames, with 5 pumwaters
	- Replace quenched, touchgroups, neargroups

- Gametime handling improvements.
	- Need to be robust in event of gametime resetting to zero,
	  e.g. env_meta.txt corruption.
	- Probably need to separate nodecore gametime entirely from
	  minetest gametime.
	- Always need to guard against the backward flow of time
	  in normal gameplay circumstances, e.g. due to loss of
	  meta files.
		- Need to store backup in world dir next to
		  env_meta.txt.
		- Partial world resets (e.g. nuking map, keeping
		  dais ex machina) should not reset time.
	- Allow customizing time flow in vanilla:
		- Time rate multipliers, default 1, for each of:
			- Server offline (os.time() adjust)
			- Server online but empty (no players online)
			- Normal gameplay
		- Probably need to track accumulators for each
		  separately in mod storage and then add them together
		  to get gametime.

- Replace digparticles with "node" and maybe "node_tile" options for
  particlespawners new in 5.3+
	- N.B. the docs imply it only works for nodes, not items,
	  so tool breakage particles may still require the logic.

- Add AISM profiler similar to how ABM profiler already works

- Optimize colony growth logic
	- For each sponge found, push into a queue
	- Process queue on step:
		- Skip all "seen" sponges
		- For each sponge, floodfill to find colony
		- Mark all sponges in colony as "seen"
	- Look for a "colony center" sponge that has colony metadata
		- Process growth for entire colony, generate sponges
			as needed
		- If a sponge is dug, check to see if it's the colony
			seed and has the metadata
		- Transfer metadata to any face-touching sponge

- Recipe "rate adjust" is messed up.
	- Make rate_adjust ONLY use config
	- Add a speed adjustment for manual changes, e.g. when
	  tool speed is used

- Simpler version of the soaking API that uses an exponential random
  distribution and mean time between events to determine if at least one
  event happened
	- Support cascading, i.e. pushing time into another event type
	  for replacement nodes?

- LookTips
	- API needs an overhaul
		- Use a single generic name everywhere (getdesc?)
		- Simplify calling, maybe pass data table
		- Unify between node/itemdefs, entdefs, etc.
	- Update mods depending on touchtips
	- Fix glyph handling to be face-specific instead of
	  always naming the glyph if above, even if wrong face
	- Crosshair still not fully synced with looktip: node
	  name visible due to punching nodes still does not
	  solidify the crosshair in darkness.

- Player skin API overhaul
	- Pass a context object only
	- Use player name, not player obj

- Use set_local_animation again?
	- Apparently it may make sense instead of handling every animation on the
	  server side, to send sets of animations for the player based on context,
	  e.g. swap between "swimup", "swimdown" and "standing/walking" sets and
	  let the client handle the parts it can handle.
	https://github.com/minetest/minetest_game/pull/2917/files#diff-bb0f88d32425a778a80208ad343f9e94880cba87893232410d8c0fba22295343R129

- Update based on recentish engine sound improvements
	- Improvements committed 2020-02-01
	- exclude_player in sound_play instead of looping through
	  https://github.com/minetest/minetest/commit/ace3c7611
	- Remove NC's "social sounds" feature for dig/place
	  https://github.com/minetest/minetest/commit/c10e18859

- Convert more things to autostart DNTs?

- Expand stackapply recipes
	- Allow them to work with non-stacks?
		- Hook into item_place
	- Work for mixing coal into wet concrete?
	- Make stickcraft recipes work in tight spaces?
		- Simulate node placement for place recipes?

- Anywhere we are using explicit names or lists of names in game, we
  should probably try registering groups to allow future expansion

- Unify the "node destruction particles" system.  Allow complex options,
  like alternative node def, particle def, disable on silk touch.

- A wooden plank appeared to have decayed in place of a scaling node;
  could it be a problem with ABM muxing?

- Switch ents to using on_step moveresult for collision detection
  instead of checking nodes directly.

- Organize into modpacks for layers
	- Compat
	- Util
	- API
	- Natural
	- Tech

- Try to detect and separate out translation strings coming from
  non-core mods so we can consistently generate the game translation
  file without having to disable mods.

- Automate uploading translation source strings?

- Test nc_wield with items that have their own visual scale.

- Flammability ABM ordering.  Always do extinguish check after ignite
  checks or vice versa, so suboptimal firestarting is consistent

- Make door pusheability customizable
	- door_can_push or door_cannot_push groups/flags or something.
	- Allow override of standard falling_node check.

- Overhaul player health to work entirely virtual
	- Raise max up a lot to make room for fine grained falling
	  damage so we can raise threshold
	- Consider falling damage add percent to all nodes?
	- Add API for doing fake damage effect using set hp

- Allow optics pending transaction to be flushed early
	- Optics interacting with doors are out of sync
	- May need to commit the optic state immediately when triggering
	  doors so they get the correct current state.

- Consider full eversion of event handlers
	- Disallow (or automatically convert) traditional single-handlers
	  on all definitions
	- Use a register_on_something() method to hook pretty much
	  everything

- Unify mkreg
	- Add validation/modifying and order comparison funcs
	- Use for recipes, mapgen_shared, playerstep, etc.
	- Consider replacing with register_generic():
		- Allow lazy registration, i.e. a register method that can
		  register for things not yet defined, and allow later registration
		  of the registration definition that will pick up those missed.
	- Define order for inv tabs

- Transactional api ref localization
	- For certain large computations, like an optic processing, it may be
	  worthwhile to capture some often-repeated functions like minetest.get_node
	  as locals.
	- May need to test performance on a non-JIT build, where this is expected
	  to have a larger impact, since JIT probably optimizes the difference away.

- Some time-integral processes are not resetting when prereqs removed
	- Cooking resumes instead of resetting when fire removed.
	- Leaching resumes instead of resetting when water removed.

- Make leaf decay group-driven for mod applicability.

- Switch some extended attributes to first-class support instead of modifying
  definition at registration time.
	- e.g. the way drop_in_place used to be an after_dig_node hook
	  modifier but became first-class.

- Add a nodecore.inherit_node() helper that registers a node as a
  modified copy of another definition, e.g. for planted eggcorns copying
  dirt, door panels copying original materials, etc.

- Need to find a way to make AISMs work with falling_node.
	- Create ItemStack, run hooks, try to convert back to node
	  (assume node and item meta map 1:1) and if it fails then
	  spawn an item_ent.
	- Need this for amalgamation remelting.
	- Would it make sense to unify the item and falling_node ents?

- Unify sponge growth logic so it can be used for mapgen, to ensure
  that spawned sponges match naturally-grown ones?

- Easier recipe API:
	- Use ascii art like ezschematic?
	- Aliases like "sides"/"corners"?
	- Make larger/complex recipes easier to define.

- API for recursing through embedded inventories, e.g. in totes.
	- Lux reaction cannot sense other lux inside totes.

- Scripts to validate dependency graph.
	- Tag deps directly in code at place of use.
	- Auto-generate mod.conf / depends.txt

- API Cleanup
	- Further nc_api break-up, clean up util functions.
	- Heat API
		- Quenched, Flames as numbers or nil?
	- Unify nc_items and visinv API.
		- Stack nodes are "special", get first-class support.
	- APIs for different kinds of "neighbor" scans
		- Face, edge, corner touching

........................................................................
========================================================================
