*This package is the **development edition** of the **NodeCore** game.*  It may include cutting-edge or experimental new features.  It may also be unstable or buggy.

Updates are released automatically, tracking the [NodeCore Dev Branch](https://gitlab.com/sztest/nodecore/commits/dev).  Note that there may be some releases that don't include *any* functionality changes, since the automated process may pick up changes to embedded documentation or other things that don't end up in the game.

Alternatively, check out the [Official Release Edition of NodeCore](/packages/Warr1024/nodecore/).