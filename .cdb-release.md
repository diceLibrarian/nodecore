*This package is the **stable** release of NodeCore.*  It's ideal for streamers, offline players, or others who can't afford to deal with bugs, but new features may be delayed.

Alternatively, check out [NodeCore ALPHA](/packages/Warr1024/nodecore_alpha/) for the cutting-edge unstable version.
